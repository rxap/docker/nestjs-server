#!/bin/sh

# print all environment variables if the environment variable DEBUG is set
if [ "$DEBUG" = "*" ]; then
  env
fi

echo "Starting Node.js application..."
echo "Args: $@"

NODE_APP_FILE=${NODE_APP_FILE:-main.js}

if [ "$NODE_APP_FILE" = "main.js" ]; then
  echo "NODE_APP_FILE is set to the default value 'main.js'"
  if [ ! -f "/app/main.js" ]; then
    echo "The file '/app/main.js' does not exist"
    if [ -f "/app/main.mjs" ]; then
      echo "The file '/app/main.mjs' exists and will be used instead of '/app/main.js'"
      NODE_APP_FILE="main.mjs"
    else
      echo "Please set the environment variable NODE_APP_FILE to the name of the main Node.js application file"
      exit 1
    fi
  fi
fi

# Check if NODE_INSPECT_ENABLED is "true"
if [ "$NODE_INSPECT_ENABLED" = "true" ]; then
    # If true, start Node.js with the --inspect flag on the specified port
    node --inspect="0.0.0.0:${NODE_INSPECT_PORT:-9229}" "/app/$NODE_APP_FILE"
else
    # If not true, start Node.js normally
    node "/app/$NODE_APP_FILE"
fi
