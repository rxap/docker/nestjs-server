# Changelog

## [0.0.1-nightly.2](https://gitlab.com/rxap/docker/nestjs-server/compare/v0.0.1-nightly.1...v0.0.1-nightly.2) (2024-06-29)

## [0.1.0-edge.1](https://gitlab.com/rxap/docker/nestjs-server/compare/v0.0.1-nightly.1...v0.0.1-nightly.2) (2024-06-28)

## [0.1.0-edge.1](https://gitlab.com/rxap/docker/nestjs-server/compare/v0.1.0-edge.0...v0.1.0-edge.1) (2024-06-28)

## [0.0.1-nightly.1](https://gitlab.com/rxap/docker/nestjs-server/compare/v0.0.1-nightly.0...v0.0.1-nightly.1) (2024-05-24)

## [0.1.0-edge.0](https://gitlab.com/rxap/docker/nestjs-server/compare/v0.0.1-nightly.0...v0.0.1-nightly.1) (2024-05-23)

## 0.1.0-edge.0 (2024-05-23)


### Features

* check if the main file exists and auto detected a mjs file ([e53d522](https://gitlab.com/rxap/docker/nestjs-server/commit/e53d522d442ce3decb9c2d869d432abbdc545062))

## 0.0.1-nightly.0 (2024-05-22)
